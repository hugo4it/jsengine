const fs = require("fs")
const path = require("path")
var projectsDir = path.join(__dirname, "Projects")

window.addEventListener('DOMContentLoaded', e => {
    const replaceText = (selector, text) => {
        const element = document.getElementById(selector)
        if (element) element.innerText = text
    }
  
    for (const type of ['chrome', 'node', 'electron']) {
        replaceText(`${type}-version`, process.versions[type])
    }

    const loc = window.location.pathname.split("/")[window.location.pathname.split("/").length-1];
    if (loc === "createProject.html") {
        const wc = document.querySelector(".window-content .list");
        const listItem = `<div class="list-item"><div class="left"><div class="top"><span>ITEM_TITLE</span></div><div class="bottom"><span>ITEM_PATH</span></div></div><div class="right"><a><svg style="width:auto;height:50px;" viewBox="0 0 11 18"><path d="M9 7C11 9 11 9 9 11L3 17C2 18 0 16 1 15L6 10C7 9 7 9 6 8L1 3C0 2 2 0 3 1Z" fill="black" /></svg></a></div></div>`;
    
        if (!fs.existsSync(projectsDir)) {
            fs.mkdirSync(projectsDir)
            window.location = "newProject.html"
        } else {
            var i = 0;
            fs.readdirSync(projectsDir).forEach(f=>{
                const file = fs.readFileSync("Projects/"+f);
                console.log(file);
                console.log(f);
                const div = document.createElement("div");
                div.innerHTML = listItem;
                div.querySelector(".list-item").style.animationDelay = ""+(6+i)+"00ms"
                wc.appendChild(div);
                i++
            })
        }
    }
})