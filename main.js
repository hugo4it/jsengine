const { app, BrowserWindow } = require('electron')
const debug = require('electron-debug')(); // Enable Debug Menu
const path = require('path')


function createWindow () {
  const win = new BrowserWindow({
    width: 1280,
    height: 720,
    title: "Loading...",
    webPreferences: {
      preload: path.join(__dirname, 'preload.js')
    }
  })

  win.setMenu(null);
  win.setTitle("Loading...");
  win.loadURL("file://"+__dirname+"/static/createProject.html");
}

app.whenReady().then(() => {
  createWindow()

  app.on('activate', () => {
    if (BrowserWindow.getAllWindows().length === 0) {
      createWindow()
    }
  })
})

app.on('window-all-closed', () => {
  if (process.platform !== 'darwin') {
    app.quit()
  }
})